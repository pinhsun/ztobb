#ifndef tree_h
#define tree_h

#include <TFile.h>
#include <TTree.h>
#include <TH1.h>
#include <TMath.h>
#include <TLorentzVector.h>

#include <iostream>

float deltaPhi(float phi1, float phi2) {

  float dPhi = phi1 - phi2;
  if (dPhi > TMath::Pi()) dPhi -= 2*TMath::Pi();
  if (dPhi <= -TMath::Pi()) dPhi += 2* TMath::Pi();

  return dPhi;

}

float deltaR(float eta1, float phi1, float eta2, float phi2) {

  float dEta = eta1 - eta2;
  float dPhi = phi1 - phi2;
  if (dPhi > TMath::Pi()) dPhi -= 2*TMath::Pi();
  if (dPhi <= -TMath::Pi()) dPhi += 2* TMath::Pi();

  return sqrt(pow(dEta,2) + pow (dPhi,2));

}

const int maxPar = 50;
Int_t run_;
Int_t lumi_;
Long64_t event_;

Int_t     trig_Ele23_Ele12;
Int_t     trig_Ele17_Ele12;
Int_t     trig_Mu17_Mu8;

Int_t 	leptType;
Float_t lept0_pt;
Float_t lept0_eta;
Float_t lept0_phi;

Float_t lept1_pt;
Float_t lept1_eta;
Float_t lept1_phi;

Float_t z_pt;
Float_t z_eta;
Float_t z_phi;
Float_t z_y;
Float_t z_mass;
Int_t   z_charge;
Int_t	_nJet;
Float_t	_jetPt;
Float_t	_jetEn;








void inittree(TTree* tree) {

  tree->Branch("run",         &run_);
  tree->Branch("lumi",        &lumi_);
  tree->Branch("event",       &event_);
  tree->Branch("trig_Ele17_Ele12",        &trig_Ele17_Ele12);
  tree->Branch("trig_Ele23_Ele12",        &trig_Ele23_Ele12);
  tree->Branch("trig_Mu17_Mu8",           &trig_Mu17_Mu8);














}

bool	hasMu;
Int_t     run;
Long64_t  event;
Int_t     lumis;
Bool_t    isData;
ULong64_t hlt;
Float_t genWeight;

Int_t nEle ;
Int_t* eleCharge;
Int_t* eleChargeConsistent;
Float_t *eleEn;
Float_t* elePt ;
Float_t* eleEta ;
Float_t* elePhi ;
Float_t* eleSCEta ;
Float_t* eleSCPhi;
Float_t* eleSCEtaWidth;
Float_t* eleSCPhiWidth;
Float_t* eleSCEn ;
Short_t* eleID;


Int_t    nMu;
Int_t*   muType;
Float_t* muPt;
Float_t* muEta;
Float_t* muPhi;
Int_t*   muCh;
Float_t* muPFChIso;
Float_t* muPFPhoIso;
Float_t* muPFNeuIso;
Float_t* muPFPUIso;
Float_t* muPFChIso03;
Float_t* muPFPhoIso03;
Float_t* muPFNeuIso03;
Float_t* muPFPUIso03;
Int_t* muIDbit;

//jet
Int_t nJet;
Float_t* jetPt;
Float_t* jetEn;
Float_t* jetEta;
Float_t* jetPhi;
Float_t* jetRawPt;
Float_t* jetRawEn;
Float_t* jetMt;
Float_t* jetArea;
Int_t* jetID;   

//gen level
Int_t nMC;
Int_t* mcPID;
Int_t* mcMomPID;
Int_t* mcGMomPID;
Float_t* mcPt;
Float_t* mcEt;
Float_t* mcPhi;
Float_t* mcEta;
Float_t* mcMomPt;
Float_t* mcMomEta;
Float_t* mcMomPhi;
Float_t* mcMomMass;
UShort_t* mcStatusFlag;
Float_t* mcCalIsoDR03;
Float_t* mcCalIsoDR04;
Float_t genPho1;
Float_t genEle1;
Float_t genEle2;
Float_t genMu1;
Float_t genMu2;
Float_t genEleEta1;
Float_t genEleEta2;
Float_t genMuEta1;
Float_t genMuEta2;
Float_t genPhoEta1;
Float_t genElePhi1;
Float_t genElePhi2;
Float_t genMuPhi1;
Float_t genMuPhi2;
Float_t genPhoPhi1;




void readggtree(TreeReader &data) {

  run = data.GetInt("run");
  event = data.GetLong64("event");
  lumis = data.GetInt("lumis");
  hlt = data.GetLong64("HLTEleMuX");

  nEle = data.GetInt("nEle");
  eleCharge = data.GetPtrInt("eleCharge");
  elePt = data.GetPtrFloat("eleCalibPt");
  eleEn = data.GetPtrFloat("eleCalibEn");
  eleEta = data.GetPtrFloat("eleEta");
  elePhi = data.GetPtrFloat("elePhi");
  eleSCEta = data.GetPtrFloat("eleSCEta");
  eleSCPhi = data.GetPtrFloat("eleSCPhi");
  eleSCEtaWidth = data.GetPtrFloat("eleSCEtaWidth");
  eleSCPhiWidth = data.GetPtrFloat("eleSCPhiWidth");
  eleSCEn = data.GetPtrFloat("eleSCEn");
  eleID = data.GetPtrShort("eleIDbit");

  nMu = data.GetInt("nMu");
  muType = data.GetPtrInt("muType");
  muPt = data.GetPtrFloat("muPt");//
  muEta = data.GetPtrFloat("muEta");
  muPhi = data.GetPtrFloat("muPhi");
  muCh = data.GetPtrInt("muCharge");
  muPFChIso = data.GetPtrFloat("muPFChIso");
  muPFNeuIso = data.GetPtrFloat("muPFNeuIso");
  muPFPhoIso = data.GetPtrFloat("muPFPhoIso");
  muPFPUIso = data.GetPtrFloat("muPFPUIso");
  muPFChIso03 = data.GetPtrFloat("muPFChIso03");
  muPFNeuIso03 = data.GetPtrFloat("muPFNeuIso03");
  muPFPhoIso03 = data.GetPtrFloat("muPFPhoIso03");
  muPFPUIso03 = data.GetPtrFloat("muPFPUIso03");
  muIDbit =  data.GetPtrInt("muIDbit");

  nJet = data.GetInt("nJet");
  jetPt		= data.GetPtrFloat("jetPt");	
  jetEn		= data.GetPtrFloat("jetEn");	
  jetEta	= data.GetPtrFloat("jetEta");
  jetPhi	= data.GetPtrFloat("jetPhi");	
  jetRawPt	= data.GetPtrFloat("jetRawPt");	
  jetRawEn	= data.GetPtrFloat("jetRawEn");	
  jetMt		= data.GetPtrFloat("jetMt");	
  jetArea	= data.GetPtrFloat("jetArea");
  jetID		= data.GetPtrInt("jetID");		



  if (data.HasMC()) {
  nMC = data.GetInt("nMC");
  mcPID = data.GetPtrInt("mcPID");
  mcMomPID = data.GetPtrInt("mcMomPID");
  mcGMomPID = data.GetPtrInt("mcGMomPID");
  mcPt = data.GetPtrFloat("mcPt");
  mcEt = data.GetPtrFloat("mcEt");
  mcPhi = data.GetPtrFloat("mcPhi");
  mcEta = data.GetPtrFloat("mcEta");
  mcMomPt = data.GetPtrFloat("mcMomPt");
  mcMomEta = data.GetPtrFloat("mcMomEta");
  mcMomPhi = data.GetPtrFloat("mcMomPhi");
  mcMomMass = data.GetPtrFloat("mcMomMass");
  mcStatusFlag = (UShort_t*) data.GetPtrShort("mcStatusFlag");
  genWeight = data.GetFloat("genWeight");
  mcCalIsoDR03 = data.GetPtrFloat("mcCalIsoDR03");
  mcCalIsoDR04 = data.GetPtrFloat("mcCalIsoDR04");

  genPho1 = data.GetFloat("genPho1");
  genEle1 = data.GetFloat("genEle1");
  genEle2 = data.GetFloat("genEle2");
  genMu1  = data.GetFloat("genMu1");
  genMu2  = data.GetFloat("genMu2");

  genPhoEta1 = data.GetFloat("genPhoEta1");
  genEleEta1 = data.GetFloat("genEleEta1");
  genEleEta2 = data.GetFloat("genEleEta2");
  genMuEta1  = data.GetFloat("genMuEta1");
  genMuEta2  = data.GetFloat("genMuEta2");

  genPhoPhi1 = data.GetFloat("genPhoPhi1");
  genElePhi1 = data.GetFloat("genElePhi1");
  genElePhi2 = data.GetFloat("genElePhi2");
  genMuPhi1  = data.GetFloat("genMuPhi1");
  genMuPhi2  = data.GetFloat("genMuPhi2");


  }

}


#endif
