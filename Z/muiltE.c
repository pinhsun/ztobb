#include "SimThr.h"
#include "MZ.h"
#include <unistd.h>
string MisStr[] = {
	        "job_DoubleMu_Run2016B_Legacy",
	        "job_DoubleMu_Run2016C_Legacy",
	        "job_DoubleMu_Run2016D_Legacy",
	        "job_DoubleMu_Run2016E_Legacy",
	        "job_DoubleMu_Run2016F_Legacy",
	        "job_DoubleMu_Run2016G_Legacy",
	        "job_DoubleMu_Run2016H_Legacy",
			"job_DoubleMu_Run2017B_MarReminiAOD",
			"job_DoubleMu_Run2017C_MarReminiAOD",
			"job_DoubleMu_Run2017D_MarReminiAOD",
			"job_DoubleMu_Run2017E_MarReminiAOD",
			"job_DoubleMu_Run2017F_MarReminiAOD",
			};
string Mis2[]={
			"job_DoubleEG_Run2016B_Legacy",
			"job_DoubleEG_Run2016C_Legacy",
			"job_DoubleEG_Run2016D_Legacy",
			"job_DoubleEG_Run2016E_Legacy",
			"job_DoubleEG_Run2016F_Legacy",
			"job_DoubleEG_Run2016G_Legacy",
			"job_DoubleEG_Run2016H_Legacy",
			"job_DoubleEG_Run2017B_MarReminiAOD",
			"job_DoubleEG_Run2017C_MarReminiAOD",
			"job_DoubleEG_Run2017D_MarReminiAOD",
			"job_DoubleEG_Run2017E_MarReminiAOD",
			"job_DoubleEG_Run2017F_MarReminiAOD",
			};
string Dir="/data1/ggNtuples/V09_04_13_03/";
const int MissLen=12;

void muiltE(){
	while(1){
		ThrNext();
		cout<<Mis<<" : Mis "<<endl;
		if(Mis>=MissLen) break;
		//printf("%s \n",MisStr[Mis].c_str());
		EZ(Dir,Mis2[Mis]);
	}
}
