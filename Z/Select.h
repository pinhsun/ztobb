#ifndef Select_h
#define Select_h
void passEleId(std::vector<int> &selectedEle) {
  selectedEle.clear();
  std::vector<int> passUnsorted;
  passUnsorted.clear();
  std::vector<float> elePtUnsorted;
  elePtUnsorted.clear();

  for (int i = 0; i < nEle; i++) {
    if (elePt[i] < 10.) continue;
    if (fabs(eleSCEta[i]) > 2.5) continue;
    if (fabs(eleSCEta[i]) > 1.4442 && fabs(eleSCEta[i]) < 1.566) continue;
    if ( ((eleID[i] >> 3) &1) == 1 ) {
      passUnsorted.push_back(i);
      elePtUnsorted.push_back(elePt[i]);
    }
  }

  //sort ele in pt descending
  int siz = (int) passUnsorted.size();
  if (siz < 1) return;

  int ind[siz];
  TMath::Sort(siz, &elePtUnsorted.front(), ind);
  for (int i = 0; i < siz; ++i) {
    selectedEle.push_back(passUnsorted[ind[i]]);
  }

}



void passMuonId (std::vector<int> &selectedMu) {
  selectedMu.clear();
  std::vector<int> passUnsorted;
  passUnsorted.clear();
  std::vector<float> muPtUnsorted;
  muPtUnsorted.clear();

  for (int i = 0; i < nMu; i++) {
    if (muPt[i] < 10.) continue;
    if (fabs(muEta[i]) > 2.4) continue;
    if ( ((muIDbit[i] >> 2) &1) != 1) continue;
    if ( (muPFChIso[i] + TMath::Max(0., muPFPhoIso[i] + muPFNeuIso[i] -0.5 * muPFPUIso[i]))/muPt[i] > 0.15) continue;
    //if ( (muPFChIso[i] + TMath::Max(0., muPFPhoIso[i] + muPFNeuIso[i] -0.5 * muPFPUIso[i]))/muPt[i] > 0.25) continue;
    passUnsorted.push_back(i);
    muPtUnsorted.push_back(muPt[i]);
  }

  //sort pt in descending
  int siz = (int) passUnsorted.size();
  if (siz < 1) return;

  int ind[siz];
  TMath::Sort(siz, &muPtUnsorted.front(), ind);
  for (int i = 0; i < siz; ++i) {
    selectedMu.push_back(passUnsorted[ind[i]]);
  }

}

void passJetId(std::vector<int> &selectedJet) {
  selectedJet.clear();
  std::vector<int> passUnsorted;
  passUnsorted.clear();
  std::vector<float> jetPtUnsorted;
  jetPtUnsorted.clear();

  for (int i = 0; i < nJet; i++) {
    if (jetPt[i] < 25.) continue;
    if (fabs(jetEta[i]) < 2.5){
      passUnsorted.push_back(i);
      jetPtUnsorted.push_back(jetPt[i]);
    }
  }

  //sort ele in pt descending
  int siz = (int) passUnsorted.size();
  if (siz < 1)  return;

  int ind[siz];
  TMath::Sort(siz, &jetPtUnsorted.front(), ind);
  for (int i = 0; i < siz; ++i) {
    selectedJet.push_back(passUnsorted[ind[i]]);
  }

}

#endif
