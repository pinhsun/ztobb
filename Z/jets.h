#include <iostream>
#include <fstream>

#include <TFile.h>
#include <TH1.h>
#include <TH2.h>
#include <TTree.h>
#include <TCanvas.h>
#include <TLorentzVector.h>
#include <TVector2.h>
#include <TF1.h>
#include <TMath.h>
#include <string>
#include "untuplizer.h"
#include "tree.h"
//#include <Select.h>
#include <vector>

using namespace std;


void readJets(string path,TH1* hJet){
	TreeReader data2(TString(path), "ggNtuplizer/EventTree");
	Long64_t ev2=data2.GetEntriesFast();
	TLorentzVector jet1, jet2, jets;
	for (Long64_t ev = 0; ev < ev2; ev++) {
		    data2.GetEntry(ev);
			readggtree(data2);
			vector<int> selectedJet;
			passJetId(selectedJet);
			if(selectedJet.size()>1){
					
				jet1.SetPtEtaPhiE(jetPt[selectedJet[0]],jetEta[selectedJet[0]],jetPhi[selectedJet[0]],jetEn[selectedJet[0]]);
				jet2.SetPtEtaPhiE(jetPt[selectedJet[1]],jetEta[selectedJet[1]],jetPhi[selectedJet[1]],jetEn[selectedJet[1]]);
				jets=jet1+jet2;

				hJet->Fill(jets.Pt());
			}
	}
}

