#include <iostream>
#include <fstream>

#include <TFile.h>
#include <TH1.h>
#include <TH2.h>
#include <TTree.h>
#include <TCanvas.h>
#include <TLorentzVector.h>
#include <TVector2.h>
#include <TF1.h>
#include <TMath.h>
#include "Dir.h"
#include "twoL.h"
#include "untuplizer.h"
#include "tree.h"
#include <vector>
#include "jets.h"
using namespace std;
void Z15(){
	TFile *MyFile = new TFile("~/GoChip01/event.root","RECREATE");
	TH1* hE = new TH1D("hE", "Two-Electron invariant mass", 90,0, 140);
	TH1* hE2 = new TH1D("hE2", "Two-Electron invariant mass", 90,0, 140);
	TH1* hM = new TH1D("hM", "Two-Moun invariant mass", 90,0, 140);
	TH1* hM2 = new TH1D("hM2", "Two-Moun invariant mass", 90,0, 140);
	TH1* hJetME = new TH1D("hJetME", "B jet invariant mass(ZtoEE)", 100,0, 15);
	TH1* hJetMMu = new TH1D("hJetMMu", "B jet invariant mass(ZtoMuMu)",100,0, 15);
	TH1* ZvBE = new TH1D("ZvBE", "b jets/Z Pt(ZtoEE)", 200,0,10);
	TH1* ZvBM = new TH1D("ZvBM", "b jets/Z Pt(ZtoMuMu)", 200,0,10);
	TH1* hJPtE = new TH1D("hJPtE", "jets Pt(ZtoEE)", 100,0,100);
	TH1* hJPtM = new TH1D("hJPtM", "jets Pt(ZtoMuMu)", 100,0,100);
	string eDir ="/data1/ggNtuples/V09_04_13_03/job_DoubleEG_Run2017B_MarReminiAOD/";

	vector<string> eFile;
	getDir(eDir,eFile);
		for(int i=0;i<15;i++){
//		for(int i=0;i<eFile.size();i++){
			cout<<i<<" E in "<<eFile.size()<<endl;
			eeToZ(eDir+eFile[i],hE,hE2,hJetME,ZvBE);
			readJets(eDir+eFile[i],hJPtE);
		}
	

	string mDir ="/data1/ggNtuples/V09_04_13_03/job_DoubleMu_Run2017B_MarReminiAOD/";

	vector<string> mFile;
	getDir(mDir,mFile);
		for(int i=0;i<15;i++){
//		for(int i=0;i<mFile.size();i++){
			cout<<i<<" M in "<<mFile.size()<<endl;
			mumuToZ(mDir+mFile[i],hM,hM2,hJetMMu,ZvBM);
	
			readJets(mDir+mFile[i],hJPtM);
		}
	hM->Write();
	hM2->Write();
	hE->Write();
	hE2->Write();
	ZvBE->Write();
	ZvBM->Write();
	hJetME->Write(); 
	hJetMMu->Write();
	h2JvZPtE->Write();
	h2JvZPtM->Write();
	hJPtE->Write();
	hJPtM->Write();
	MyFile->Close();
	
}
