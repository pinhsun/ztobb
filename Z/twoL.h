#include <iostream>
#include <fstream>

#include <TFile.h>
#include <TH1.h>
#include <TH2.h>
#include <TTree.h>
#include <TCanvas.h>
#include <TLorentzVector.h>
#include <TVector2.h>
#include <TF1.h>
#include <TMath.h>
#include <string>
#include "untuplizer.h"
#include "tree.h"
#include "Select.h"
#include <vector>

using namespace std;

TH2* h2JvZPtE = new TH2F("h2JvZPtE","2d Jets v Z Pt",100,0.0,100.0,100,0.0,100.0);
TH2* h2JvZPtM = new TH2F("h2JvZPtM","2d Jets v Z Pt",100,0.0,100.0,100,0.0,100.0);

void eeToZ(string path,TH1* hE,TH1* hE2,TH1* hJetE,TH1* ZvBPt){

	TreeReader data1(TString(path), "ggNtuplizer/EventTree");
	TLorentzVector ele1, ele2, Z, jet1, jet2, jets;
	Long64_t ev1 = 0;
	Long64_t ev2 = -1;
	if (ev2 < 0) ev2 = data1.GetEntriesFast();
	if (ev2 > data1.GetEntriesFast()) ev2 = data1.GetEntriesFast();
	for (Long64_t ev = ev1; ev < ev2; ++ev) {
		    data1.GetEntry(ev);
			readggtree(data1);
			vector<int> eleIndex;
			vector<int> selectedEle;
			if(!(hlt>>40&1)&&!(hlt>>5&1)) continue;
			passEleId(selectedEle);

			if(selectedEle.size()>1&&elePt[0]>25.){
				
				ele1.SetPtEtaPhiM(elePt[selectedEle[0]], eleEta[selectedEle[0]], elePhi[selectedEle[0]], 0.000511);
				ele2.SetPtEtaPhiM(elePt[selectedEle[1]], eleEta[selectedEle[1]], elePhi[selectedEle[1]], 0.000511);
				Z=ele1+ele2;
				if(Z.M()<50.) continue;
				hE->Fill(Z.M());
				vector<int> selectedJet;
				passJetId(selectedJet);
				if(selectedJet.size()>1){
					
					jet1.SetPtEtaPhiE(jetPt[selectedJet[0]],jetEta[selectedJet[0]],jetPhi[selectedJet[0]],jetEn[selectedJet[0]]);
					jet2.SetPtEtaPhiE(jetPt[selectedJet[1]],jetEta[selectedJet[1]],jetPhi[selectedJet[1]],jetEn[selectedJet[1]]);
					jets=jet1+jet2;

					hE2->Fill(Z.M());
					hJetE->Fill(jet1.M());
					hJetE->Fill(jet2.M());
					ZvBPt->Fill((jets.Pt()/Z.Pt()));
					h2JvZPtE->Fill(jets.Pt(),Z.Pt());
				}
		}
	}
}
void mumuToZ(string path,TH1* hM,TH1* hM2,TH1* hJetM,TH1* ZvBPt){
	TreeReader data2(TString(path), "ggNtuplizer/EventTree");
	Long64_t ev2=data2.GetEntriesFast();
	TLorentzVector Mu1, Mu2, Z, jet1, jet2, jets;
	for (Long64_t ev = 0; ev < ev2; ev++) {
		    data2.GetEntry(ev);
			readggtree(data2);
			if(!(hlt>>14&1)) continue;
			vector<int> eleIndex;
			vector<int> selectedMu;
			passMuonId(selectedMu);

			if(selectedMu.size()>1&&muPt[0]>20.){
				Mu1.SetPtEtaPhiM(muPt[selectedMu[0]], muEta[selectedMu[0]], muPhi[selectedMu[0]], 0.104);
				Mu2.SetPtEtaPhiM(muPt[selectedMu[1]], muEta[selectedMu[1]], muPhi[selectedMu[1]], 0.104);
				Z=Mu1+Mu2;
				if(Z.M()<50.) continue;
				hM->Fill(Z.M());
				vector<int> selectedJet;
				passJetId(selectedJet);
				if(selectedJet.size()>1){
					
					jet1.SetPtEtaPhiE(jetPt[selectedJet[0]],jetEta[selectedJet[0]],jetPhi[selectedJet[0]],jetEn[selectedJet[0]]);
					jet2.SetPtEtaPhiE(jetPt[selectedJet[1]],jetEta[selectedJet[1]],jetPhi[selectedJet[1]],jetEn[selectedJet[1]]);
					jets=jet1+jet2;

					hM2->Fill(Z.M());
					hJetM->Fill(jet1.M());
					hJetM->Fill(jet2.M());
					ZvBPt->Fill((jets.Pt()/Z.Pt()));
					h2JvZPtM->Fill(jets.Pt(),Z.Pt());
				}
		}
	}
}
