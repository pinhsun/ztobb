#include <iostream>
#include <fstream>

#include <TFile.h>
#include <TH1.h>
#include <TH2.h>
#include <TTree.h>
#include <TCanvas.h>
#include <TLorentzVector.h>
#include <TVector2.h>
#include <TF1.h>
#include <TMath.h>

#include "untuplizer.h"
#include "tree.h"
#include "ElectronSelection.h"
#include "MuonSelection.h"
#include <vector>
using namespace std;
int Z(){

	TreeReader data1("/data1/ggNtuples/V09_04_13_03/job_DoubleEG_Run2017B_MarReminiAOD/ggtree_data_1.root", "ggNtuplizer/EventTree");
	TH1* hE = new TH1D("hE", "Two-Electron invariant mass", 90,0, 140);
	TH1* hM = new TH1D("hM", "Two-Moun invariant mass", 90,0, 140);
	TCanvas* c =new TCanvas("c","",1200,800);
	Long64_t ev1 = 0;
	Long64_t ev2 = -1;
	if (ev2 < 0) ev2 = data1.GetEntriesFast();
	if (ev2 > data1.GetEntriesFast()) ev2 = data1.GetEntriesFast();
	cout<<ev2<<endl;
	for (Long64_t ev = ev1; ev < ev2; ++ev) {
		    data1.GetEntry(ev);
			readggtree(data1);
			vector<int> eleIndex;
			vector<int> selectedEle;

			passEleId(selectedEle);

			if(selectedEle.size()>1){
				Float_t* EelePt    = data1.GetPtrFloat("elePt");
				Float_t* EeleEta   = data1.GetPtrFloat("eleEta");
				Float_t* EelePhi   = data1.GetPtrFloat("elePhi");
				TLorentzVector ele1, ele2;
				ele1.SetPtEtaPhiM(elePt[selectedEle[0]], eleEta[selectedEle[0]], elePhi[selectedEle[0]], 0.000511);
				ele2.SetPtEtaPhiM(elePt[selectedEle[1]], eleEta[selectedEle[1]], elePhi[selectedEle[1]], 0.000511);
				TLorentzVector Z = ele1 + ele2;
//				cout<<Z.M()<<endl;
				hE->Fill(Z.M());
		}
	}
	TreeReader data2("/data1/ggNtuples/V09_04_13_03/job_DoubleMu_Run2017B_MarReminiAOD/ggtree_data_1.root", "ggNtuplizer/EventTree");
	ev2=data2.GetEntriesFast();
	cout<<ev2<<endl;
	for (Long64_t ev = ev1; ev < ev2; ++ev) {
		    data2.GetEntry(ev);
			readggtree(data2);
			vector<int> eleIndex;
			vector<int> selectedMu;

			NoMuonId(selectedMu);

			if(selectedMu.size()>1){
				TLorentzVector Mu1, Mu2;
				Mu1.SetPtEtaPhiM(muPt[selectedMu[0]], muEta[selectedMu[0]], muPhi[selectedMu[0]], 0.104);
				Mu2.SetPtEtaPhiM(muPt[selectedMu[1]], muEta[selectedMu[1]], muPhi[selectedMu[1]], 0.104);
				TLorentzVector Z = Mu1 + Mu2;
				hM->Fill(Z.M());
		}
	}
	c->Divide(2);
	c->cd(1);
	hM->Draw();
	c->cd(2);
	hE->Draw();
}
