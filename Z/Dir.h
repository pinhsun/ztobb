/**
    Theme: List a Directory
    compiler: Dev C++ 4.9.9.2
    Date: 100/04/16
    Author: ShengWen
    Blog: https://cg2010studio.wordpress.com/
*/

#include <sys/types.h>
#include <dirent.h>
#include <errno.h>
#include <vector>
#include <string>
#include <iostream>
bool RootOrNot(string name){
	unsigned long i=name.rfind(".");
	if(i!=name.npos){
		string s2 = ".ROOT";
		if(strcasecmp(name.assign(name, i, (name.size()-i)).c_str(),s2.c_str()) == 0){
			return true;
		}
		return false;
	}
	else{
		return false;
	}
}
int getDir(string dir, vector<string> &files){
    DIR *dp;//創立資料夾指標
    struct dirent *dirp;
    if((dp = opendir(dir.c_str())) == NULL){
        cout << "Error(" << errno << ") opening " << dir << endl;
        return errno;
    }
    while((dirp = readdir(dp)) != NULL){//如果dirent指標非空
		if(RootOrNot(string(dirp->d_name))) files.push_back(string(dirp->d_name));//將資料夾和檔案名放入vector
    }
    closedir(dp);//關閉資料夾指標
    return 0;
}

